import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import './ProgressBar.css'
const BorderLinearProgress = withStyles((theme) => ({
    root: {
      height: 15,
      width: 800,
      borderRadius: 5,
    },
    colorPrimary: {
      backgroundColor: 'rgba(237, 171, 238, 0.438)',
    },
    bar: {
      borderRadius: 5,
      backgroundColor: 'rgba(61,38,103,1)',
    },
  }))(LinearProgress);
const progress = [14,38,64,88];

const ProgressBar = (props) => {
    console.log(props);
    return (
        <div class='progress-bar'>
            <BorderLinearProgress variant="determinate" value={progress[props.current]} />
            <div className='progress-legend'>
                {props.progress.map((element, i) => {
                    console.log(element.type)
                    const style = props.current !== i ? {backgroundColor: 'rgba(237, 171, 238)'} : {backgroundColor: 'rgba(61,38,103,1)'} ;
                    return (
                        <div key={i} className='progress-item'>
                            <div className='circle' style={style} />
                            <span>{element.type}</span>
                        </div>
                    )
                })}
            </div>
        </div>
    );
};


export default ProgressBar;

//style={(props.current === i) ? color: 'rgba(237, 171, 238, 0.438)' : color: 'rgba(61,38,103,1)'}
import React from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import logo from '../../assets/img/icon-logo.png';

const Navbar = () => {
  const { pathname } = window.location

  return (
    <div className='nav-container'>
      <div className='nav-text-btn'>
        <Link to='/'>
          <img src={logo} alt='str' />
        </Link>
        <Link style={{ textDecoration: 'none', color: pathname === '/' ? '#333' : 'whitesmoke' }} to='about'>
          Sobre
        </Link>
        <Link
          style={{ textDecoration: 'none', color: pathname === '/' ? '#333' : 'whitesmoke' }}
          to='volunteers'>
          Versão Beta
        </Link>
      </div>
      <div className='nav-btn'>
        <button style={{ color: pathname === '/' ? '#333' : 'whitesmoke', textDecoration: 'none' }}>
          <strong>Login</strong>
        </button>
        <button style={{ color: pathname === '/' ? '#333' : 'whitesmoke', textDecoration: 'none' }} className='btn-signup'>
          <strong>Sign Up</strong>
        </button>
      </div>
    </div>
  );
};

export default Navbar;

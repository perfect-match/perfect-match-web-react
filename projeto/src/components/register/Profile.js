import React from 'react';

const Profile = (props) => {
    return (
        <div>
            <h1>Perfil</h1>
            <button onClick={()=> {
                props.setProProgress(0);                
            }}>Anterior</button>
            <button onClick={()=> {
                props.setProProgress(2);                
            }}>Proximo</button>
        </div>
    );
};

export default Profile;
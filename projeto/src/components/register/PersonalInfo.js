import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core/styles';
import './Register.css'
const PerfectTextField = withStyles({
    root: {
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: 'rgba(237, 171, 238, 0.438)',
        },
        '&:hover fieldset': {
          borderColor: 'rgba(237, 171, 238)',
        },
        '&.Mui-focused fieldset': {
          borderColor: 'rgba(61,38,103,1)',
        },
      },
    },
  })(TextField);

const PersonalInfo = (props) => {
    //Um pra cada Campo
    // useState(oque vai aqui dentro eh o tipo e o valor inicia)
    let [name, setName] = useState("");
    let [errorName, setErrorName] = useState(false);
    let [email, setEmail] = useState("");
    let [errorEmail, setErrorEmail] = useState(false);
    return (
        <div style={{marginTop: '4rem', width: '800px', display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <div style={{ display: 'flex'}}>
                <div style={{marginRight: '20px'}}>
                    {/*aqui vai os inputes da coluna um */}
                    <PerfectTextField
                        style={{width: '380px'}}
                        error={errorName}
                        label="nome"
                        variant="outlined"
                        value={name}
                        helperText={errorName ? 'campo obrigatorio' : ''}
                        onChange={(e)=> {
                            setName(e.target.value);
                            if(errorName){
                                setErrorName(false);
                            }                   
                        }}
                    />
                </div>
                <div style={{marginLeft: '20px'}}>
                    {/*aqui vai os inputes da coluna dois */}
                    <PerfectTextField
                        style={{width: '380px'}}
                        error={errorEmail}
                        label="email"
                        variant="outlined"
                        value={email}
                        helperText={errorEmail ? 'campo obrigatorio' : ''}
                        onChange={(e)=> {
                            setEmail(e.target.value);
                            if(errorEmail){
                                setErrorEmail(false);
                            }                   
                        }}
                    />
                </div>
            </div>
           
            <button onClick={()=> {
                //Aqui pode ser feita mais verificacoes
                //nesse caso estamos so verificando se o compo foi preenchido
                if (!name) {
                    setErrorName(true);
                }
                else if(!email) {
                    setErrorEmail(true);
                }
                else {
                    props.setProProgress(1);
                }
            }}>Proximo</button>
        </div>
    );
};
export default PersonalInfo;
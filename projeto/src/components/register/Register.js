import React, {useState} from 'react';
import ProgressBar from '../progressRegisterBar/ProgressBar';
import PersonalInfo from './PersonalInfo';
import Profile from './Profile';
import Intrests from './Intrests';
import Expectations from './Expectations'
const prog = [
    {
        type: 'Informações Pessoais', 
        isSet: true
    },
    {
        type: 'Perfil', 
        isSet: false
    },
    {
        type: 'Gostos', 
        isSet: false
    },
    {
        type: 'Expectativas', 
        isSet: false
    },
];
const Register = () => {
    const [progress, setProgress] = useState(0)
    return (
        <div style={{width: '90vw' ,display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
            <h1>Cadastro</h1>
            <ProgressBar progress={prog} current={progress}/>
            {progress === 0 && <PersonalInfo setProProgress={setProgress}/>}
            {progress === 1 && <Profile  setProProgress={setProgress}/>}
            {progress === 2 && <Intrests  setProProgress={setProgress}/>}
            {progress === 3 && <Expectations  setProProgress={setProgress}/>}
        </div>
    );
};

export default Register;
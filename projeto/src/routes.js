import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Home from './pages/home/Home';
import About from './pages/about/About';
import Volunteers from './pages/volunteers/Volunteers';
import Teste from './pages/teste';

const Routes = () => (
  <BrowserRouter>
    <Route exact path='/' component={Home} />
    <Route path='/about' component={About} />
    <Route path='/volunteers' component={Volunteers} />
    <Route path='/teste' component={Teste} />
    <Route path='*' /> {/* Fazer o comonent Not-found */}
  </BrowserRouter>
);

export default Routes;

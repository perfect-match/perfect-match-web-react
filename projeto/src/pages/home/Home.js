import React from 'react';
import { Link } from 'react-router-dom';
import Navbar from '../../components/navbar/Navbar';
import './Home.css';
import ImgHome from '../../assets/img/home-page-straight-couple.png';

const Home = () => {
  return (
    <div>
      <Navbar />
      <div className='home-container'>
        <img alt='couple-illustration' src={ImgHome} />

        <div className='home-info'>
          <h1>Perfect Match</h1>
          <span>
            Pare de colecionar match e <br />
            encontre seu Verdadeiro Par!
          </span>
          <Link to='about'>
            <button className='home-button'><strong>Saiba Mais</strong></button>
          </Link>
        </div>
      </div>

    </div>
  );
};

export default Home;

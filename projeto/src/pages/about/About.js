import React from 'react';
import ImgAbout from '../../assets/img/about-page-illustration.png';
import Navbar from '../../components/navbar/Navbar';
import './About.css';
import Gabriel from '../../assets/img/dev-gabriel.png';
import Barbara from '../../assets/img/dev-barbara.png';
import Joao from '../../assets/img/dev-joao.png';
import Felipe from '../../assets/img/dev-felipe.png';
import Hilton from '../../assets/img/dev-hilton.png';
import Davi from '../../assets/img/dev-davi.png';
import Lucas from '../../assets/img/dev-lucas.png';

const About = () => {
  return (
    <div className='container'>
      <Navbar />
      <div className='about-container'>
        <img className="illustration" alt='couples' src={ImgAbout} />
        
        <div>
          <h1>Sobre Nós</h1>
          <p>
            Somos um aplicativo revolucionário de relacionamento. <br/> Visando uma
            melhor experiência e conforto, nós usamos suas informações
            cadastradas para calcular compatibilidade e mostrar para você opções
            de match's perfeitos. <br /> Para todos os tipos de amores; pessoas e
            relacionamentos!!
          </p>
          <h1>Desenvolvedores</h1>
          <div className='row'>
            <img src={Gabriel} alt="dev-gabriel" />
            <img src={Barbara} alt="dev-barbara" />
            <img src={Joao} alt="dev-joao" />
            <img src={Felipe} alt="dev-felipe" />
          </div>
          <div className='row'>
            <img src={Hilton} alt="dev-hilton" />
          <img src={Davi} alt="dev-davi" />
          <img src={Lucas} alt="dev-lucas" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;

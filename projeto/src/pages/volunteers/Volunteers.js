import React from 'react';
import Navbar from '../../components/navbar/Navbar';
import ImgVolunteers from '../../assets/img/volunteers-page-illustration.png';
import './Volunteers.css';

const Volunteers = () => {
  return (
    <div className='container'>
      <Navbar />
      <div className='volunteers-container'>
        <div className='volunteers-info'>
          <h3>procuramos</h3>
          <h1>Voluntários</h1>
          <span>
            Fazer um algoritmo de compatibilidade não é fácil para que nós
            possamos ser cada vez mais precisos em calcular o amor precisamos de
            você! Junte-se a nós nessa jornada, Se inscrevendo na versão beta !
            <br />
          </span>
          <button className='volunteers-button'>Cadastre-se</button>
        </div>
        <img alt='couples' src={ImgVolunteers} />
      </div>
    </div>
  );
};
export default Volunteers;
